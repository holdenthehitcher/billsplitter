var allPeople = [];
var paymentType = "total";
var completeTotal;
var totalAmount = 0;
var taxAmount = 0;
var tipAmount = 0;

//by total or perItem

$(document).ready(function () {
  // on click addName function for turning names into created objects

  $("#names").on("click", "#nameButton", function addName() {
    var newName = $("#nameInput").val();
    if (newName == "") {
      alert("Please enter a name");
    } else if (allPeople.indexOf(newName) === -1) {
      allPeople.push(newName);
      $("#namesList").append("<li>" + newName + "</li>");
    } else {
      alert("Choose a different name");
    }
  });

  //function for "Calculate" button action
  $("#calculate").on("click", "#calculateChoice", function () {
    if ($("#byItems").is(":checked")) {
      paymentType = "perItem";
    }
    if ($("byTotal").is(":checked")) {
      paymentType = "total";
    }
  });

  //Getting Total Amount $ from Input

  $("#total").on("click", "#submitTotal", function () {
    totalAmount = parseFloat($("#totalAmount").val()).toFixed(2);
    completeTotal = totalAmount;
    if (isNaN(totalAmount)) {
      alert("Enter a valid number for your amount");
    } else if (totalAmount <= 0) {
      alert("Enter an amount greater than $0");
    } else {
      return printTotal(completeTotal);
    }
  });

  function printTotal(amount) {
    return $("#totalText").html("<b>$</b>" + "<b>" + amount + "</b>");
  }

  //get input and calculate tax

  $("#taxDiv").on("click", "#submitTax", function () {
    let taxPercent = parseFloat($(".taxInput").val()) / 100;
    totalAmount = parseFloat(totalAmount);
    taxAmount = taxPercent * totalAmount;
    if (totalAmount == 0) {
      alert("Fill in the Total Amount above to continue");
    } else if (isNaN(taxPercent)) {
      alert("Enter a valid amount");
    } else if (taxPercent < 0) {
      alert("Enter an amount greater than 0");
    } else {
      completeTotal = totalAmount + taxAmount + tipAmount;
      return printTotal(completeTotal);
    }
  });
  //calculate tip

  $("#tipDiv").on("click", "#submitTip", function () {
    totalAmount = parseFloat(totalAmount);
    tipPercent = parseFloat($(".tipInput").val()) / 100;
    tipAmount = totalAmount * tipPercent;
    if (totalAmount == 0) {
      alert("Fill in the Total Amount above to continue");
    } else if (isNaN(tipPercent) || tipPercent < 0) {
      alert("Enter a valid amount");
    } else {
      completeTotal = totalAmount + taxAmount + tipAmount;
      return printTotal(completeTotal);
    }
  });

  //function for pay amount input for each person - make array

  //function for displaying amount owed

  /*
    function createTaxElement(){
        let element = document.createElement("p");
        element.innerHTML = "Tax percent:";
        let input = document.createElement("input");
        input.setAttribute("type", "button");
        Object.assign(input, {
            className: 'input',
            onclick: addTip()
        });
        let taxDom = document.getElementById('taxInput');
        document.taxDom.appendChild(element).appendChild(input);
    } 
  





});

/*
    



}); 


/*  ----Adding names to a list section----

    $("#names").on("click", "addName", function (){
        var nameItems=[]; 
        $("#names").find("input", "textarea", "addName").each(function(index, Person){
            nameItems.push($('<li/>').text(Person.title));
        });
        $("#nameList").append.apply($("#nameList"), nameItems);
    });
*/

  /*    ----Calculate section-----

    var button = getElementById("button");
function click(){
    alert("Can this work please");
}


    function addItems(){

}

    function getTotal(){

}
*/
});
