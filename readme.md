{
    "people":[{
        id:"unique string",
        photo:"profile photo",
        accounts:[{
            id:"unique string",
            username:"string",
            type:"venmo, paypal, etc."
        }],
    }],
    "receipt":{
        tax:"number"
        tip:"number"
        items:[{
        name:"string",
        id:"unique string",
        price:"number",
        quantity:"number",
    }]}
}
